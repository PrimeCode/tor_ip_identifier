import os
import subprocess
from urllib.request import urlretrieve as urt

def getFile():
	print ("Downloading File Tor_query_EXPORT.csv this may take 5-10 seconds.")
	urt('https://torstatus.blutmagie.de/query_export.php/Tor_query_EXPORT.csv', 'hosts.csv')
	print ("File Successfully Downloaded.")

def hostfile():
	process = subprocess.Popen("csvcut -c 5 hosts.csv", stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = process.communicate()
	
	file = open("hostips.txt", "wb")
	file.write(stdout)

def createArray(main):
	hostip = open("hostips.txt", "a")
	fileEOF = hostip.tell()
	hostip.close()

	hostip = open("hostips.txt", "r")
	
	while(hostip.tell() != fileEOF):
		test = hostip.readline().split("\n")[0]

		if(test == main):
			print (test, main)

	hostip.close()

if __name__ == "__main__":
	if("hosts.csv" not in os.listdir()):
		getFile()
	else:
		print ("Already Present hosts.csv")
	if("hostips.txt" not in os.listdir()):	
		hostfile()
	else:
		print ("Already Present hostips.txt")
	createArray("94.214.188.154")